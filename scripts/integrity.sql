
-- before insert
delimiter //
create trigger before_employee_insert 
	before insert on employee
	for each row
begin
	if new.post != ALL((select post from post)) or new.pharmacy_id != ALL((select id from pharmacy))
	then 
	signal sqlstate '45000'
	set message_text = 'Insertion Error! Foreign keys do not match!' ;
	end if;
end//
delimiter ;

-- before update
delimiter //
create trigger before_update_employee
	before update on employee
	for each row
begin
 	if new.pharmacy_id != any((select id from pharmacy)) or new.post != any((select post from post))
 	then
 	signal sqlstate '45000'
 	set message_text = 'Update Error!';
 	end if;
end //
delimiter ;


#---post-------------------------------------------------------------------------------------------------------

-- before delete
delimiter //
create trigger before_delete_post
	before delete on post
	for each row
begin
	if old.post = any((select post from employee))
	then 
	signal sqlstate '45000'
	set message_text = 'Delete Error!';	
	end if;
end //
delimiter ;


#---pharmacy-------------------------------------------------------------------------------------------------------

-- before insert
delimiter //
create trigger before_insert_pharmacy
	before insert on pharmacy
	for each row
begin
	if new.street != any((select street from street))
	then 
	signal sqlstate '45000'
	set message_text = 'Insertion Error! Foreign keys does not match!';
	end if;
end //
delimiter ;

-- before update
delimiter //
create trigger before_update_pharmacy
	before update on pharmacy
	for each row
begin
	if new.street != any((select street from street))
	then
	signal sqlstate '45000'
	set message_text = 'Update Error!';
	end if;
end //
delimiter ;

-- before delete
delimiter //
create trigger before_delete_pharmacy
	before delete on pharmacy
	for each row
begin
	if old.id = any((select pharmacy_id from employee)) or old.id = any((select pharmacy_id from pharmacy_medicine))
	then
	signal sqlstate '45000'
	set message_text = 'Delete Error!';
	end if;
end //
delimiter ;


#---street-------------------------------------------------------------------------------------------------------

-- before delete
delimiter //
create trigger before_delete_street
	before delete on street
	for each row
begin
	if old.street = any((select street from pharmacy))
	then
	signal sqlstate '45000'
	set message_text = 'Delete Error!'; 
	end if;
end //
delimiter ;



#---medicine-------------------------------------------------------------------------------------------------------

-- before delete
delimiter //
create trigger before_medicine_delete
	before delete on medicine
	for each row
begin
	if old.id = any((select medicine_id from medicine_zone)) or old.id = any((select medicine_id from pharmacy_medicine))
	then
	signal sqlstate '45000'
	set message_text = 'Delete Error!';
	end if;
end //
delimiter ;


#---pharmacy_medicine-------------------------------------------------------------------------------------------------------

-- before insert
delimiter //
create trigger before_insert_pharmacy_medicine
	before insert on pharmacy_medicine
	for each row
begin
	if new.pharmacy_id != any((select id from pharmacy)) or new.medicine_id != any((select id from medicine))
	then 
	signal sqlstate '45000'
	set message_text = 'Insertion Error! Foreign keys does not match!';
	end if;
end //
delimiter ;

-- before update
delimiter //
create trigger before_update_pharmacy_medicine
before update on pharmacy_medicine
for each row
begin
	if new.pharmacy_id != any((select id from pharmacy)) or new.medicine_id != any((select id from medicine))
	then
	signal sqlstate '45000'
	set message_text = 'Update Error!';
	end if;
end //
delimiter ;


#---zone-------------------------------------------------------------------------------------------------------

-- before delete
delimiter //
create trigger before_delete_zone
	before delete on zone
	for each row
begin
	if old.id = any((select zone_id from medicine_zone))
	then
	signal sqlstate '45000'
	set message_text = 'Delete Error!';
	end if;
end //
delimiter ;


#---medicine_zone-------------------------------------------------------------------------------------------------------

-- before insert
delimiter //
create trigger before_insert_medicine_zone
	before insert on medicine_zone
	for each row
begin 
	if new.medicine_id != any((select id from medicine)) or new.zone_id != any((select id from zone))
	then 
	signal sqlstate '45000'
	set message_text = 'Insertion Error! Foreign keys does not match!';
	end if;
end //
delimiter ;

-- before update
delimiter //
create trigger before_update_medicine_zone
	before update on medicine_zone
	for each row
begin
if new.medicine_id != any((select id from medicine)) or new.zone_id != any((select id from zone))
	then
	signal sqlstate '45000'
	set message_text = 'Update Error!';
	end if;
end //
delimiter ;