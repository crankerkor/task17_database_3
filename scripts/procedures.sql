#procedure for inserting employee
DELIMITER //
CREATE PROCEDURE parametrized_employee_insert (IN new_surname varchar(30),
IN new_name char(30), IN new_middle_name varchar(30),
IN new_identity_number char(10),
IN new_experience decimal(10,1),
IN new_post varchar(15),
IN new_pharmacy_id int(11))
BEGIN
INSERT INTO employee(surname, name, midle_name, identity_number, experience, post, pharmacy_id)
values(new_surname, new_name, new_middle_name, new_identity_number, new_experience, new_post, new_pharmacy_id);
END //
DELIMITER ;

#for inserting proper values in medicine_zone
DELIMITER //
CREATE PROCEDURE insert_medicine_zone(in medicine_id int, in zone_id int)
BEGIN
	IF medicine_id = any(select id from medicine) and zone_id = any(select id from zone)
    THEN INSERT INTO medicine_zone VALUES(medicine_id, zone_id);
    ELSE SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = "ERROR ON INSERT";
    END IF;
END //
DELIMITER ;