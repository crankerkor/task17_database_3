#---zone------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ 
insert into zone(name) values("heart");
insert into zone(name) values("kidneys");
insert into zone(name) values("lungs");
insert into zone(name) values("stomach");

#---medicine------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ 
insert into medicine(name, ministry_code, recipe, narcotic, psychotropic) values("Panadol", "1234", true, false, false);
insert into medicine(name, ministry_code, recipe, narcotic, psychotropic) values("Aspirin", "1342", false, false, false);
insert into medicine(name, ministry_code, recipe, narcotic, psychotropic) values("Sulfacid", "2334", true, false, true);
insert into medicine(name, ministry_code, recipe, narcotic, psychotropic) values("Fenistiril", "4534", false, false, false);
insert into medicine(name, ministry_code, recipe, narcotic, psychotropic) values("Albucid", "5234", true, false, false);
insert into medicine(name, ministry_code, recipe, narcotic, psychotropic) values("Noshpa", "6234", true, false, true);
insert into medicine(name, ministry_code, recipe, narcotic, psychotropic) values("Menoza", "1534", false, false, false);
insert into medicine(name, ministry_code, recipe, narcotic, psychotropic) values("Albucid", "2234", true, false, false);
insert into medicine(name, ministry_code, recipe, narcotic, psychotropic) values("Noshpa", "7234", true, false, true);	

#---street------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ 
insert into street values("Shevchenko str");
insert into street values("Patona str");
insert into street values("Melnyka str");
insert into street values("Chornovola str");
insert into street values("Zelena str");

#---pharmacy------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ 
insert into pharmacy(name, building_number, www, work_time, saturday, sunday, street) values("Zelena Apteka", 76, "www.zelena.com", '9:00', true, false, "Shevchenko str");
insert into pharmacy(name, building_number, www, work_time, saturday, sunday, street) values("DS", 76, "www.ds.com", '7:00', true, true, "Patona str");
insert into pharmacy(name, building_number, www, work_time, saturday, sunday, street) values("Apotecary", 76, "www.apotecary.com", '10:00', true, false, "Melnyka str");
insert into pharmacy(name, building_number, www, work_time, saturday, sunday, street) values("Med", 76, "www.med.com", '10:00', true, false, "Chornovola str");
insert into pharmacy(name, building_number, www, work_time, saturday, sunday, street) values("Medservice", 76, "www.medservice.com", '10:00', true, false, "Zelena str");

#---post------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ 
insert into post values("Consultant");
insert into post values("Pharmacist");
insert into post values("Assistant");

#---employee------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ 
insert into employee(surname, name, midle_name, identity_number, experience, post, pharmacy_id) values("Melnyk", "Roman", "Vasylovych", 4235422432, 5, '1950-11-11', "Pharmacist", 1);
insert into employee(surname, name, midle_name, identity_number, experience, post, pharmacy_id) values("Onysko", "Sergiy", "Petrovych", 3235422565, 3, '1950-11-11', "Consultant", 2);
insert into employee(surname, name, midle_name, identity_number, experience, post, pharmacy_id) values("Perec", "Olga", "Serhiivna", 4483820543, 5, '1950-11-11', "Pharmacist", 1);
insert into employee(surname, name, midle_name, identity_number, experience, post, pharmacy_id) values("Pochynok", "Maria", "Vasylivna", 7890735443, 3, '1950-11-11', "Assistant", 2);
insert into employee(surname, name, midle_name, identity_number, experience, post, pharmacy_id) values("Verhola", "Petro", "Vasylovych", 3235644324, 2, '1950-11-11', "Consultant", 5);
insert into employee(surname, name, midle_name, identity_number, experience, post, pharmacy_id) values("Polic", "Bohdan", "Vasylovych", 2335644324, 2, '1950-11-11', "Consultant", 1);
insert into employee(surname, name, midle_name, identity_number, experience, post, pharmacy_id) values("Serena", "Marta", "Vasylivna", 5435644324, 2, '1950-11-11', "Assistant", 2);

#---medicine_zone------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ 
insert into medicine_zone(medicine_id, zone_id) values(1, 2);
insert into medicine_zone(medicine_id, zone_id) values(2, 1);
insert into medicine_zone(medicine_id, zone_id) values(1, 3);
insert into medicine_zone(medicine_id, zone_id) values(2, 4);
insert into medicine_zone(medicine_id, zone_id) values(7, 1);
insert into medicine_zone(medicine_id, zone_id) values(9, 2);
insert into medicine_zone(medicine_id, zone_id) values(1, 4);

#---pharmacy_medicine------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ 
insert into pharmacy_medicine(pharmacy_id, medicine_id) values(1, 1);
insert into pharmacy_medicine(pharmacy_id, medicine_id) values(2, 7);
insert into pharmacy_medicine(pharmacy_id, medicine_id) values(4, 9);
insert into pharmacy_medicine(pharmacy_id, medicine_id) values(1, 5);
insert into pharmacy_medicine(pharmacy_id, medicine_id) values(3, 4);
insert into pharmacy_medicine(pharmacy_id, medicine_id) values(2, 2);
insert into pharmacy_medicine(pharmacy_id, medicine_id) values(4, 3);
insert into pharmacy_medicine(pharmacy_id, medicine_id) values(5, 2);
insert into pharmacy_medicine(pharmacy_id, medicine_id) values(3, 6);