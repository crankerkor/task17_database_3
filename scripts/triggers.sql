DELIMITER //
CREATE TRIGGER before_insert_identity_number_check
BEFORE INSERT 
ON employee FOR EACH ROW
BEGIN 
	IF NEW.identity_number RLIKE "00$"
    THEN SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = "identity_number mustn't end with two 00";
    END IF;
END //
CREATE TRIGGER before_update_identity_number_check
BEFORE INSERT 
ON employee FOR EACH ROW
BEGIN 
	IF NEW.identity_number RLIKE "00$"
    THEN SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = "identity_number mustn't end with two 00";
    END IF;
END //

CREATE TRIGGER before_insert_medicine_check_ministry_code
BEFORE INSERT
ON medicine FOR EACH ROW
BEGIN
IF new.ministry_code NOT RLIKE "^[a-z]&&[^mp]{2}-[0-9]{3}-[0-9]{2}"
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = "ministry code has to be different";
END IF;
END //
CREATE TRIGGER before_update_medicine_check_ministry_code
BEFORE UPDATE
ON medicine FOR EACH ROW
BEGIN
IF new.ministry_code NOT RLIKE "^[a-z]&&[^mp]{2}-[0-9]{3}-[0-9]{2}"
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = "ministry code has to be different";
END IF;
END //
CREATE TRIGGER post_update_restriction
BEFORE UPDATE
ON post FOR EACH ROW
BEGIN
SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = "post table is restricted for modification";
END //
CREATE TRIGGER post_delete_restriction
BEFORE DELETE
ON post FOR EACH ROW
BEGIN
SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = "post table is restricted for modification";
END //
DELIMITER ;
